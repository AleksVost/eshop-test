﻿using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;

namespace AzureFuncBlob
{
    public static class OrderItemsReserver
    {
        [FunctionName("OrderItemsReserver")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                var connectionString = $"DefaultEndpointsProtocol=https;AccountName=eshopm9blob;AccountKey=NPrygwu0ovFPcpJ0mh5EHHFX+Anlef9EVDKv7VmFiQDydx+xIGOyCZ+e9V6YdGbJ14AFoLPj55PMMkj6ev3h2Q==;EndpointSuffix=core.windows.net";
                //var connectionString = $"";
                var storageAccount = CloudStorageAccount.Parse(connectionString);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("ordersblob");
                await container.CreateIfNotExistsAsync();


                var blobName = $"Order_{Guid.NewGuid()}.json";
                var appendBlob = container.GetAppendBlobReference(blobName);
                if (!await appendBlob.ExistsAsync())
                {
                    await appendBlob.CreateOrReplaceAsync();
                }

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                await appendBlob.UploadTextAsync(requestBody);

                return new OkObjectResult("Order successfully created");
            }
            catch (Exception e)
            {
                log.LogInformation(e.Message);
            }

            return new BadRequestObjectResult($"Order doesn't create. The number of attempts is exceeded.");
        }
    }
}
