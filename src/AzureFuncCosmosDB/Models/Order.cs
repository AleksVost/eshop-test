﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace AzureFunctions.Models
{
    public class Order
    {
        public Order()
        {
            Id = Guid.NewGuid().ToString();
            Items = new List<OrderItem>();
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Address { get; set; }
        public double FinalPrice { get; set; }
        public List<OrderItem> Items { get; set; }
    }
}
