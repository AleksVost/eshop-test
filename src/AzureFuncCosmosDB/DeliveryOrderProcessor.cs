using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Messaging.ServiceBus;
using AzureFunctions.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureFuncCosmosDB
{
    public static class DeliveryOrderProcessor
    {
        [FunctionName("DeliveryOrderProcessor")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            try
            {
                //JSON part
                var jsonString = String.Empty;
                using (StreamReader sr = new StreamReader(req.Body))
                {
                    jsonString = sr.ReadToEnd();
                }

                var order = JsonConvert.DeserializeObject<Order>(jsonString);
                //---

                //DB part
                var databaseId = "eshopm9cosmosdb";
                var containerId = "OrdersDB";
                var endpointUri = "https://eshopm9cosmosdb.documents.azure.com:443/";
                var primaryKey = "81fd3uZsdMR9gEqC06gksPkqkWz2qcXB7AtLTZjVTw25TWWufmMZCs3ST5A5JqeCUGrWKpou5obk3xB2YDzDlw==";
                var cosmosClient = new CosmosClient(endpointUri, primaryKey);
                var database = cosmosClient.CreateDatabaseIfNotExistsAsync(databaseId).Result.Database;
                var container = database.CreateContainerIfNotExistsAsync(containerId, "/OrderContainer").Result.Container;
                //---

                //Cosmos DB part
                var response = await container.CreateItemAsync<Order>(order);
                return new OkObjectResult($"Created item in database with id: {response.Resource.Id} Operation consumed {response.RequestCharge} RUs.\n");
                //---
            }
            catch (CosmosException ex)
            {
                return new BadRequestObjectResult(new { Msg = $"Order wasn't created. {ex.Message}" });
            }
        }
    }
}
